package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.Purchase;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper.IPurchaseMapper;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.repository.PurchaseRepository;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service.template.EntityService;
import cz.cvut.fit.eja.vizeldim.data.layer.IPurchaseService;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.PurchaseDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PurchaseService extends EntityService<Purchase, PurchaseDTO> implements IPurchaseService {
    @Inject
    public PurchaseService(IPurchaseMapper mapper,
                           PurchaseRepository purchaseRepository) {
        this.mainRepository = purchaseRepository;
        this.mapper = mapper;
        this.cl = PurchaseDTO.class;
    }
}
