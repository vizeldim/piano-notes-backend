package cz.cvut.fit.eja.vizeldim.data.layer;

import cz.cvut.fit.eja.vizeldim.data.layer.dto.NoteDetailDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.page.PageDto;
import cz.cvut.fit.eja.vizeldim.data.layer.paging.Page;
import cz.cvut.fit.eja.vizeldim.data.layer.template.IEntityService;
import io.smallrye.mutiny.Uni;


public interface INoteDetailService extends IEntityService<NoteDetailDTO> {
    Uni<PageDto<NoteDetailDTO>> getPageByName(Page validPage, String name);

    Uni<PageDto<NoteDetailDTO>> getPageByCategory(Page page, Long categoryId);
}
