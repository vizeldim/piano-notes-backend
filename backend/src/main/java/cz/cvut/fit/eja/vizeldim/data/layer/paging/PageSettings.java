package cz.cvut.fit.eja.vizeldim.data.layer.paging;

public class PageSettings {
    private final int defaultSize;
    private final int maxSize;

    public PageSettings() {
        this.defaultSize = 10;
        this.maxSize = 100;
    }

    public PageSettings(int defaultSize, int maxSize) {
        this.defaultSize = defaultSize;
        this.maxSize = maxSize;
    }

    public int getSize(int size) {
        return size <= 0 ? this.defaultSize : Math.min(size, this.maxSize);
    }

    public int getPage(int page) {
        return page <= 0 ? 1 : page;
    }

    public Page transform(Page page) {
        return new Page(getPage(page.getPage()), getSize(page.getSize()));
    }
}
