package cz.cvut.fit.eja.vizeldim.resource;

import cz.cvut.fit.eja.vizeldim.data.layer.ICategoryService;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.CategoryDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.page.PageDto;
import cz.cvut.fit.eja.vizeldim.data.layer.paging.Page;
import cz.cvut.fit.eja.vizeldim.resource.template.EntityResource;
import io.smallrye.mutiny.Uni;
import org.jboss.resteasy.reactive.RestResponse;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.UriInfo;


@Path("categories")
public class CategoryResource extends EntityResource<CategoryDTO> {
    private final ICategoryService categoryDao;

    @Inject
    public CategoryResource(ICategoryService categoryDao) {
        super(categoryDao, CategoryResource.class);
        this.categoryDao = categoryDao;
    }

    @Override
    public Uni<RestResponse<PageDto<CategoryDTO>>> getPage(int page, int size, UriInfo uriInfo) {
        String name = uriInfo.getQueryParameters().getFirst("name");

        Page _page = new Page(page, size);

        if (name != null) {
            return categoryDao
                    .getPageByName(_page, name)
                    .map(RestResponse::ok);
        }

        return super.getPage(page, size, null);
    }
}
