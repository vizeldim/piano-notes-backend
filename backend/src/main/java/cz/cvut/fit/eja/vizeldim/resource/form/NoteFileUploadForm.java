package cz.cvut.fit.eja.vizeldim.resource.form;

import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;

public class NoteFileUploadForm {
    @RestForm("notesFile")
    public FileUpload file;
}
