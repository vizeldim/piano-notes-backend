package cz.cvut.fit.eja.vizeldim.data.layer;

import cz.cvut.fit.eja.vizeldim.data.layer.auth.AuthDto;
import cz.cvut.fit.eja.vizeldim.data.layer.auth.UserDto;
import io.smallrye.mutiny.Uni;

public interface IUserService {
    Uni<UserDto> getByEmail(String email);

    Uni<UserDto> verify(AuthDto authDto);
}
