package cz.cvut.fit.eja.vizeldim.data.layer.auth;

public enum RoleDto {
    CUSTOMER, ADMIN
}
