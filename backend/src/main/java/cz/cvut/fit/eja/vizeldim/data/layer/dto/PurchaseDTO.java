package cz.cvut.fit.eja.vizeldim.data.layer.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;

import java.time.LocalDateTime;

@RegisterForReflection
public class PurchaseDTO extends BaseDto {
    public final LocalDateTime date;
    public final String email;

    public PurchaseDTO(Long id, LocalDateTime date, String email) {
        this.id = id;
        this.date = date;
        this.email = email;
    }
}
