package cz.cvut.fit.eja.vizeldim.resource;

import cz.cvut.fit.eja.vizeldim.data.layer.IFileService;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.FileDTO;
import cz.cvut.fit.eja.vizeldim.resource.form.NoteFileDownloadForm;
import cz.cvut.fit.eja.vizeldim.resource.form.NoteFileUploadForm;
import io.smallrye.mutiny.Uni;
import org.jboss.resteasy.reactive.MultipartForm;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.math.BigDecimal;
import java.util.Objects;

@Path("/files")
@Produces(MediaType.APPLICATION_JSON)
public class FileResource {
    IFileService service;

    @Inject
    public FileResource(IFileService service) {
        this.service = service;
    }

    @POST
    @RolesAllowed({"ADMIN"})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Uni<Response> upload(@MultipartForm NoteFileUploadForm noteFile) {
        if (!Objects.equals(noteFile.file.contentType(), "application/pdf")) {
            noteFile.file.uploadedFile().toFile().delete();
            return Uni.createFrom().item(Response.status(Response.Status.BAD_REQUEST).build());
        }

        String fileName = noteFile.file.uploadedFile().getFileName().toString();
        String fileFormat = "pdf";
        FileDTO fileDTO = new FileDTO(fileName, new BigDecimal(5), true, fileFormat, 13L);
        return service
                .save(fileDTO)
                .map(x -> Response.ok(x).build());
    }

    @GET
    @RolesAllowed({"ADMIN"})
    @Produces(MediaType.MULTIPART_FORM_DATA)
    @Path("/{id}")
    public Uni<File> getFile(@PathParam("id") long id) {
        return service
                .getById(id)
                .map(f -> {
                    File nf = new File("./files/" + f.name);

                    NoteFileDownloadForm downloadForm = new NoteFileDownloadForm();
                    downloadForm.name = f.name;
                    downloadForm.file = nf;

                    Response.ResponseBuilder response = Response.ok(nf);
                    response.header("Content-Disposition", "attachment;filename=" + f.name + ".pdf");

                    return nf;
                });
    }
}
