package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.repository;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.Preview;
import io.quarkus.hibernate.reactive.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;


@ApplicationScoped
public class PreviewRepository implements PanacheRepository<Preview> {
}
