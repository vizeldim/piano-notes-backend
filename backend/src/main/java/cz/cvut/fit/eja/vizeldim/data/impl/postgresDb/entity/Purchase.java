package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
public class Purchase extends BaseEntity {
    private LocalDateTime date;
    private String email;

    @ManyToMany
    private List<File> files;
}
