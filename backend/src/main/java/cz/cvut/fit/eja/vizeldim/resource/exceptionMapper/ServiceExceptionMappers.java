package cz.cvut.fit.eja.vizeldim.resource.exceptionMapper;

import cz.cvut.fit.eja.vizeldim.data.layer.exception.MyDbException;
import cz.cvut.fit.eja.vizeldim.data.layer.exception.MyNotFoundException;
import io.smallrye.mutiny.Uni;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ServiceExceptionMappers {
    @ServerExceptionMapper
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<RestResponse<String>> mapException(MyNotFoundException e) {
        return Uni.createFrom().item(RestResponse.status(Response.Status.NOT_FOUND));
    }

    @ServerExceptionMapper
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<RestResponse<String>> mapException(MyDbException e) {
        return Uni.createFrom().item(RestResponse.status(Response.Status.BAD_GATEWAY));
    }
}
