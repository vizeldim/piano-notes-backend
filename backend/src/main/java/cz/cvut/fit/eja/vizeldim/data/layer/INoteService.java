package cz.cvut.fit.eja.vizeldim.data.layer;

import cz.cvut.fit.eja.vizeldim.data.layer.dto.NoteDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.template.IEntityService;

public interface INoteService extends IEntityService<NoteDTO> {
}
