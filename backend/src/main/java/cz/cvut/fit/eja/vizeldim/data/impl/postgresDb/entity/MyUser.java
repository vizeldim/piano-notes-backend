package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NamedQuery(name = "MyUser.FindByEmail", query = "Select u from MyUser u where u.email = ?1")
@Table(name = "my_user")
public class MyUser {
    @Id
    @GeneratedValue
    private Long id;
    private String email;

    private String password;

    public MyUser(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public MyUser() {
    }

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "my_user_role", joinColumns = @JoinColumn(name = "my_user_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Set<Role> roles = new HashSet<>();
}
