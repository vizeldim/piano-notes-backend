package cz.cvut.fit.eja.vizeldim.data.layer.dto;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.view.Views;
import io.quarkus.runtime.annotations.RegisterForReflection;

import java.util.Collections;
import java.util.List;

@RegisterForReflection
public class NoteDetailDTO extends BaseDto {
    @JsonView({Views.NoteDetailPage.class, Views.NoteDetail.class})
    public String name;

    @JsonView({Views.NoteDetail.class})
    public String originalUrl;

    @JsonView({Views.NoteDetail.class})
    public Integer originalTonality;

    @JsonView({Views.NoteDetailPage.class, Views.NoteDetail.class})
    public String preview;

    @JsonView({Views.NoteDetail.class})
    public List<NoteDTO> notes = Collections.emptyList();

    @JsonView({Views.NoteDetail.class})
    public List<CategoryDTO> categories = Collections.emptyList();


    public NoteDetailDTO(Long id,
                         String name,
                         String preview) {
        this.id = id;
        this.name = name;
        this.preview = preview;
    }
}
