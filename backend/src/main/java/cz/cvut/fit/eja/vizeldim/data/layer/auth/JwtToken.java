package cz.cvut.fit.eja.vizeldim.data.layer.auth;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JwtToken {
    public String accessToken;
    public String refreshToken;
}

