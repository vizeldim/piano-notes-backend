package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.Note;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper.INoteMapper;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.repository.NoteRepository;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service.template.EntityService;
import cz.cvut.fit.eja.vizeldim.data.layer.INoteService;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.NoteDTO;
import io.smallrye.mutiny.Uni;
import org.hibernate.reactive.mutiny.Mutiny;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;


@ApplicationScoped
public class NoteService extends EntityService<Note, NoteDTO> implements INoteService {
    @Inject
    public NoteService(INoteMapper mapper,
                       NoteRepository noteRepository) {
        this.cl = NoteDTO.class;

        this.mainRepository = noteRepository;
        this.mapper = mapper;
    }

    @Override
    protected Uni<Note> addDeps(Uni<Note> noteUni) {
        return noteUni
                .call(x -> Mutiny.fetch(x.getPreviews()))
                .call(x -> Mutiny.fetch(x.getFiles()));
    }
}
