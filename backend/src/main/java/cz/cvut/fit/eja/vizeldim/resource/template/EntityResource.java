package cz.cvut.fit.eja.vizeldim.resource.template;

import cz.cvut.fit.eja.vizeldim.data.layer.dto.BaseDto;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.page.PageDto;
import cz.cvut.fit.eja.vizeldim.data.layer.paging.Page;
import cz.cvut.fit.eja.vizeldim.data.layer.template.IEntityService;
import io.smallrye.mutiny.Uni;
import org.jboss.resteasy.reactive.RestResponse;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;


@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public abstract class EntityResource<Dto extends BaseDto> {
    private final Class<?> resourceClass;

    protected IEntityService<Dto> dao;

    public EntityResource(IEntityService<Dto> dao,
                          Class<?> resourceClass) {
        this.dao = dao;
        this.resourceClass = resourceClass;
    }

    // READ
    @GET
    @Path("{id}")
    public Uni<RestResponse<Dto>> getById(@PathParam("id") Long id) {
        return dao.getById(id).map(RestResponse::ok);
    }

    // CREATE
    @POST
    @RolesAllowed({"ADMIN"})
    public Uni<Response> create(@Valid @NotNull Dto dto) {
        Uni<Long> id = dao.save(dto).map(x -> x.id);
        Uni<URI> location = id.map(x -> UriBuilder
                .fromResource(resourceClass)
                .path("/{id}")
                .resolveTemplate("id", x)
                .build());

        return location.map(x -> Response.created(x).build());
    }

    // UPDATE
    @PATCH
    @Path("{id}")
    @RolesAllowed({"ADMIN"})
    public Uni<Response> update(
            @PathParam("id") Long id,
            @NotNull Dto updateDto) {
        return dao.update(id, updateDto).map(x -> Response.noContent().build());
    }

    // READ PAGE
    @GET
    public Uni<RestResponse<PageDto<Dto>>> getPage(@QueryParam("page") @DefaultValue("1") int page,
                                                   @QueryParam("size") @DefaultValue("10") int size,
                                                   @Context UriInfo uriInfo) {
        return dao.getPage(new Page(page, size)).map(RestResponse::ok);
    }
}