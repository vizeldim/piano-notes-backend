package cz.cvut.fit.eja.vizeldim.resource.form;

import org.jboss.resteasy.reactive.PartType;
import org.jboss.resteasy.reactive.RestForm;

import javax.ws.rs.core.MediaType;
import java.io.File;

public class NoteFileDownloadForm {
    @RestForm
    public String name;

    @RestForm
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    public File file;
}