package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.Category;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.CategoryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "CDI")
public interface ICategoryMapper extends IMapper<Category, CategoryDTO> {
}
