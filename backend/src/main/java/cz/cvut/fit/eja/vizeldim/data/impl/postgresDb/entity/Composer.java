package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@Getter
@Setter
public class Composer extends BaseEntity {
    private String name;

    @ManyToMany //(mappedBy = "composers")
    private List<NoteDetail> noteDetails;
}
