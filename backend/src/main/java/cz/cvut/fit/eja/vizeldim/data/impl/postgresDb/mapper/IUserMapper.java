package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.MyUser;
import cz.cvut.fit.eja.vizeldim.data.layer.auth.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "CDI")
public interface IUserMapper {
    UserDto toDto(MyUser user);

    @Mapping(target = "id", ignore = true)
    MyUser toEntity(UserDto userDto);
}
