package cz.cvut.fit.eja.vizeldim.data.layer.auth;

public interface IJwtAuthService {
    JwtToken generate(UserDto user);

    JwtToken refresh(long expirationTime, UserDto userDto, JwtToken jwtDto);
}
