package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.decomposition.CategoryNoteDetail;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Set;

@Entity
@Getter
@Setter
@NamedQuery(name = "Category.searchAllByName", query = "Select c from Category c where lower(trim(both ' ' from c.name)) like concat('%', ?1, '%')")
@NamedQuery(name = "Category.countAllByName", query = "Select count(c) from Category c where lower(trim(both ' ' from c.name)) like concat('%', ?1, '%')")
public class Category extends BaseEntity {
    @NotNull
    private String name;

    @OneToMany(mappedBy = "category")
    @ToString.Exclude
    private Set<CategoryNoteDetail> noteDetailsBinding = Collections.emptySet(); // Collection<CategoryNoteDetail> (framework will choose the best option)
}