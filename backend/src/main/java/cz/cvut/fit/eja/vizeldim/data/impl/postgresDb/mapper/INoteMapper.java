package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.Note;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.NoteDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "CDI")
public interface INoteMapper extends IMapper<Note, NoteDTO> {
    @Override
    @Mapping(source = "noteDetail.id", target = "noteDetailId")
    NoteDTO toDto(Note note);
}