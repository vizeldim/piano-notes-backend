package cz.cvut.fit.eja.vizeldim.resource;

import cz.cvut.fit.eja.vizeldim.data.layer.IPurchaseService;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.PurchaseDTO;
import cz.cvut.fit.eja.vizeldim.resource.template.EntityResource;

import javax.inject.Inject;
import javax.ws.rs.Path;

@Path("purchases")
public class PurchaseResource extends EntityResource<PurchaseDTO> {
    private final IPurchaseService purchaseDao;

    @Inject
    public PurchaseResource(IPurchaseService purchaseDao) {
        super(purchaseDao, PurchaseResource.class);
        this.purchaseDao = purchaseDao;
    }
}
