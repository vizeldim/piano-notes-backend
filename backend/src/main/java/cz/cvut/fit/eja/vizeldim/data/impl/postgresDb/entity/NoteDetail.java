package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.decomposition.CategoryNoteDetail;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NamedQuery(name = "NoteDetail.searchAllByName", query = "Select nd from NoteDetail nd where lower(trim(both ' ' from nd.name)) like concat('%', ?1, '%')")
@NamedQuery(name = "NoteDetail.countAllByName", query = "Select count(nd) from NoteDetail nd where lower(trim(both ' ' from nd.name)) like concat('%', ?1, '%')")
@NamedQuery(name = "NoteDetail.searchByCategory", query = "select nd from NoteDetail nd join nd.categoriesBinding c where c.category.id = ?1")
@NamedQuery(name = "NoteDetail.countByCategory", query = "select count(nd) from NoteDetail nd join nd.categoriesBinding c where c.category.id = ?1")
@NamedQuery(name = "NoteDetail.findByIdWithDeps", query = "select nd from NoteDetail nd left join fetch nd.categoriesBinding left join fetch nd.notes as notes" +
                                                            " left join fetch notes.files left join fetch notes.previews where nd.id = ?1")
public class NoteDetail extends BaseEntity {
    private String name;
    private String originalUrl;

    private Integer originalTonality;

    private String preview;

    @OneToMany(mappedBy = "noteDetail")
    private List<Note> notes = Collections.emptyList();

    @OneToMany(mappedBy = "noteDetail")
    public Set<CategoryNoteDetail> categoriesBinding = Collections.emptySet();

    @ManyToMany(mappedBy = "noteDetails")
    private List<Composer> composers = Collections.emptyList();

    public void addNote(Note note) {
        this.notes.add(note);
    }
}
