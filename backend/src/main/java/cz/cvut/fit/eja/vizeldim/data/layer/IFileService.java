package cz.cvut.fit.eja.vizeldim.data.layer;

import cz.cvut.fit.eja.vizeldim.data.layer.dto.FileDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.template.IEntityService;

public interface IFileService extends IEntityService<FileDTO> {
}
