package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.File;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper.IFileMapper;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.repository.FileRepository;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service.template.EntityService;
import cz.cvut.fit.eja.vizeldim.data.layer.IFileService;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.FileDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class FileService extends EntityService<File, FileDTO> implements IFileService {
    @Inject
    public FileService(IFileMapper mapper,
                       FileRepository fileRepository) {
        this.cl = FileDTO.class;

        this.mainRepository = fileRepository;
        this.mapper = mapper;
    }
}
