package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@Entity
@Getter
@Setter
public class File extends BaseEntity {
    private String name;
    private BigDecimal price;
    private Boolean published;
    private String format;

    @ManyToOne
    @JoinColumn(name = "note_id")
    private Note note;

    @ManyToMany(mappedBy = "files")
    private List<Purchase> purchases = Collections.emptyList();

    public void addPurchase(Purchase purchase) {
        this.purchases.add(purchase);
    }
}
