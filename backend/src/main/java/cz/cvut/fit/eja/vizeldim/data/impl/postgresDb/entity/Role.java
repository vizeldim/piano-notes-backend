package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity;

public enum Role {
    CUSTOMER, ADMIN
}
